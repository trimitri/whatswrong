import pandas as pd
from units import UNITS as u


class Database:

    def __init__(self, file_name: str) -> None:
        self.db = pd.read_csv('./Grunddaten.csv', index_col='ID', usecols=range(11))
        self._log = set()  # Values that have been accessed.

    def is_equal(self, qty_a, qty_b, allowable_error: float = 1e-3) -> bool:
        """Are the passed quantities equal (or very very similar)?"""
        return abs(qty_a / qty_b - 1) < allowable_error

    @property
    def values_log(self) -> object:
        """Return log of accessed variables."""
        return self.db.loc[self._log]

    def eject_log(self) -> object:
        """Return and clear log of accessed variables."""
        log = self.values_log.sort_values('ID')
        self._log.clear()
        return log

    def id(self, record_id: int) -> object:
        """Get database record by ID."""
        record = self.db.loc[record_id]
        self._log.add(record_id)
        quantity = float(record['Value']) * u(record['Unit'])

        # Add additional info as the what the value represents.
        quantity.comment = record['Comment']
        quantity.name = "{}: {}: {} ({})".format(
            record['Cluster'], record['Category'], record['Parameter'], record['Scope'])
        quantity.source = record['Source']
        quantity.type = record['Type']
        quantity.year = record['Year']

        return quantity
