import pint

UNITS = pint.UnitRegistry()
UNITS.default_format = '.2f'
UNITS.define('€ = [currency] = eur')
UNITS.define('@alias € = euro = EUR')
UNITS.define('c = .01 * eur = cent')
UNITS.define('person = 1 count')
UNITS.define('kilogram_oil_equivalent = 0.001 * toe = kgoe')
